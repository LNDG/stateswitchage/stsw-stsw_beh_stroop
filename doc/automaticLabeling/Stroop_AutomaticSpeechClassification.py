# -*- coding: utf-8 -*-
"""
Attempt to automatically classify speech
adpated from https://realpython.com/python-speech-recognition/

How to set up Google Cloud:
    https://stackoverflow.com/questions/38703853/how-to-use-google-speech-recognition-api-in-python

export GOOGLE_APPLICATION_CREDENTIALS="/Users/kosciessa/Programming/api-key.json"
export GOOGLE_APPLICATION_CREDENTIALS=/Users/kosciessa/Programming/api-key.json
export GCLOUD_PROJECT="jqk-IAP"

putenv(GOOGLE_APPLICATION_CREDENTIALS=/Users/kosciessa/Programming/api-key.json);

"""

GOOGLE_CLOUD_SPEECH_CREDENTIALS = []

import speech_recognition as sr

r = sr.Recognizer()

rootPath = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/stroop/A_CARL/B_data/D_wavFiles/'

data = '1105_1'

DetectedWords = [None]*81
for trialID in range(1,82):
    with sr.AudioFile(rootPath+data+'/Trial'+str(trialID).zfill(2)+'.wav') as source:
        #r.adjust_for_ambient_noise(source,duration=0.25)
        audio = r.record(source)
        try:
            tempWord = r.recognize_google_cloud(audio, language='de-DE',credentials_json=GOOGLE_CLOUD_SPEECH_CREDENTIALS, show_all=False, preferred_phrases=["grün", "blau", "rot"])
            tempWord = r.recognize_google_cloud(audio, language='de-DE',credentials_json=GOOGLE_CLOUD_SPEECH_CREDENTIALS, show_all=True, preferred_phrases=["grün", "blau", "rot"])
            DetectedWords[trialID-1] = tempWord
            del tempWord
        except:
            print("Nothing detected for Trial "+str(trialID).zfill(2)+". Continuing...")
            DetectedWords[trialID-1] = ''
            pass

# WRITE indicated words into textfile

pathname = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/stroop/A_CARL/B_data/E_automaticWords/'
WriteFile = open(pathname+data+'.txt', 'w')
for item in DetectedWords:
  WriteFile.write("%s\n" % item)
WriteFile.close()
        
# Encode more information

DetectedWords   = [None]*81
Confidence      = [None]*81
StartTime       = [None]*81
EndTime         = [None]*81

for trialID in range(1,5):
    with sr.AudioFile(rootPath+data+'/Trial'+str(trialID).zfill(2)+'.wav') as source:
        #r.adjust_for_ambient_noise(source,duration=0.25)
        audio = r.record(source)
        try:
            tempWord = r.recognize_google_cloud(audio, language='de-DE',credentials_json=GOOGLE_CLOUD_SPEECH_CREDENTIALS, show_all=True, preferred_phrases=["grün", "blau", "rot"])
            DetectedWords[trialID-1] = tempWord['results'][0]['alternatives'][0]['transcript']
            Confidence[trialID-1] = tempWord['results'][0]['alternatives'][0]['confidence']
            StartTime[trialID-1] = tempWord['results'][0]['alternatives'][0]['words'][0]['startTime']
            EndTime[trialID-1] = tempWord['results'][0]['alternatives'][0]['words'][0]['endTime']
            del tempWord
        except:
            print("Nothing detected for Trial "+str(trialID).zfill(2)+". Continuing...")
            DetectedWords[trialID-1]    = None
            Confidence[trialID-1]       = None
            StartTime[trialID-1]        = None
            EndTime[trialID-1]          = None
            pass

# WRITE indicated words into textfile

pathname = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/stroop/A_CARL/B_data/E_automaticWords/'

WriteFile = open(pathname+data+'_words.txt', 'w')
for item in DetectedWords:
  WriteFile.write("%s\n" % item)
WriteFile.close()

WriteFile = open(pathname+data+'_conf.txt', 'w')
for item in Confidence:
  WriteFile.write("%s\n" % item)
WriteFile.close()

WriteFile = open(pathname+data+'_start.txt', 'w')
for item in StartTime:
  WriteFile.write("%s\n" % item)
WriteFile.close()

WriteFile = open(pathname+data+'_end.txt', 'w')
for item in EndTime:
  WriteFile.write("%s\n" % item)
WriteFile.close()

# OLD attempts
    
#tempResult = r.recognize_google_cloud(audio, language='de-DE',credentials_json=GOOGLE_CLOUD_SPEECH_CREDENTIALS, show_all=True, preferred_phrases=["grün", "blau", "rot"])
#tempResult[trialID] = r.recognize_google_cloud(audio, language='de-DE',credentials_json=GOOGLE_CLOUD_SPEECH_CREDENTIALS, show_all=False, preferred_phrases=["grün", "blau", "rot"])
#r.recognize_google_cloud(audio, language='de-DE',credentials_json=GOOGLE_CLOUD_SPEECH_CREDENTIALS, show_all=True)

#r.recognize_google_cloud(audio, language='de-DE',credentials_json=GOOGLE_CLOUD_SPEECH_CREDENTIALS, show_all=True)
#r.recognize_google_cloud(audio, language='de-DE', show_all=True, preferred_phrases=["Grün"])

#r.recognize_google(audio, language='de-DE', show_all=True)

#r.recognize_sphinx(audio, keyword_entries=[("Grün", 1.0), ("Blau", 1.0), ("Rot", 1.0)])