% Sanity-check that speech onsets start after labeled onset

pn.study = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/stroop/A_CARL/';

StateSwitchDynIDs = {1117;1118;1120;1124;1125;1126;1131;1132;1135;1136;1138;1144;1151;...
    1158;1160;1163;1164;1167;1169;1172;1173;1178;1182;1213;1214;1215;1216;...
    1219;1221;1223;1227;1228;1233;1234;1237;1239;1240;1243;1245;1247;1250;...
    1252;1257;1258;1261;1265;1266;1268;1270;1276;1281;2104;2107;2108;2112;...
    2118;2120;2121;2123;2124;2125;2129;2130;2131;2132;2133;2134;2135;2139;...
    2140;2142;2145;2147;2149;2157;2160;2201;2202;2203;2205;2206;2209;2210;...
    2211;2213;2214;2215;2216;2217;2219;2222;2224;2226;2227;2236;2237;2238;...
    2241;2244;2246;2248;2250;2251;2252;2253;2254;2255;2258;2261};

StateSwitchDynIDs = cellfun(@num2str, StateSwitchDynIDs, 'un', 0);

% load RT data
RTdata = load(['/Volumes/LNDG-1/Projects/StateSwitch/dynamic/data/behavior/stroop/A_CARL/B_data/SummaryData.mat'], 'data');

for indID = 1:numel(StateSwitchDynIDs)
    for indPeriod = 1:2

        disp(['Processing subject ', StateSwitchDynIDs{indID},' Part ', num2str(indPeriod)]);
        
        ID =  StateSwitchDynIDs{indID};
        
        pn.IN = [pn.study, 'B_data/B_DataRaw/',ID, '_StroopData_',num2str(indPeriod), '.mat'];
        if exist(pn.IN)
            audio = load(pn.IN);
        else
            disp('File does not exist');
            continue
        end

        TrialAudio = audio.StroopAudio.audio;
        
        % RT-lock speech signals
        
        idx_curID = find(ismember(RTdata.data.IDs, ID));
        RTs = RTdata.data.(['StroopOnset',num2str(indPeriod)])(:,idx_curID);
        RTs = RTs.*44100;
        
        for indTrial = 1:numel(TrialAudio)
           % pad segment by 2s
           curAudio = cat(2,TrialAudio{indTrial},NaN(2,2*44100));
           if ~isnan(RTs(indTrial)) & RTs(indTrial)-44100 > 0
                AudioRTlocked(indID,indPeriod,indTrial,:) = squeeze(nanmean(curAudio(:,RTs(indTrial)-44100: RTs(indTrial)+44100),1));
           end
        end
        
    end
end

% median power across trials
AudioRTlocked = squeeze(nanmedian(AudioRTlocked.^2,3));

pn.sanityOut = '/Volumes/LNDG-1/Projects/StateSwitch/dynamic/data/behavior/stroop/A_CARL/B_data/';
save([pn.sanityOut, 'E_audioRTlocked'], 'AudioRTlocked')

%% plot speech signal power at RT onset

addpath('/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc')

time = -1:1/44100:1;
h = figure;  
cla; hold on;
plotData = nanmean(AudioRTlocked,2);
plotData = smoothts(squeeze(plotData),'b',44);
plotData = zscore(plotData,[],2);
%plot(time, squeeze(nanmean(zscore(plotData,[],2),1)), 'Color', [.6 .6 .6], 'LineWidth',1.5)
standError = nanstd(plotData,1)./sqrt(size(plotData,1));
shadedErrorBar(time,nanmean(plotData,1),standError, ...
    'lineprops', {'color', [.6 .6 .6],'linewidth', 1.5}, 'patchSaturation', .25);
line([0,0],[-10,10], 'Color', 'k','LineWidth', 2, 'LineStyle', ':')
xlabel('Time from indicated speech onset')
ylabel('Speech signal power (z-score)')
ylim([-.1 .7]); xlim([-.25 .5])
set(findall(gcf,'-property','FontSize'),'FontSize',18)

pn.plotFolder = '/Volumes/LNDG-1/Projects/StateSwitch/dynamic/data/behavior/stroop/A_CARL/C_figures/';
figureName = 'E_sanityCheckSpeechOnset';
saveas(h, [pn.plotFolder, figureName], 'png');

%% plot by subject

figure;  
cla;
imagesc(time, [],zscore(squeeze(nanmean(AudioRTlocked,2)),[],2),[-2,2])
%line([0,0],[-10,10], 'Color', 'k','LineWidth', 2, 'LineStyle', ':')
xlabel('Time from indicated speech onset')
ylabel('Speech signal power')
%ylim([.0166, .0172])
set(findall(gcf,'-property','FontSize'),'FontSize',18)
colormap(gray)