%% get labeled data

pn.study = '/Volumes/LNDG-1/Projects/StateSwitch/dynamic/data/behavior/stroop/A_CARL/';
pn.dataFolder = [pn.study,'B_data/'];

VPs = dir([pn.dataFolder, 'B_DataRaw/1*']);
VPs = {VPs(:).name}';
VPs = cellfun(@(x){x(1:4)}, VPs);
VPs(strcmp(VPs,'1111')) = []; % remove subject that did not continue
VPs = VPs(1:2:end);

for indID = 1:numel(VPs)
    for indPeriod = 1:2

        disp(['Processing subject ', VPs{indID},' Part ', num2str(indPeriod)]);
        
        %% load raw data with experimental structure

        load([pn.dataFolder, 'B_DataRaw/',VPs{indID},'_StroopData_',num2str(indPeriod),'.mat']);

        %% load labeled data
        
        curFile = dir([pn.dataFolder, 'C_DataLabeled/',VPs{indID},'_stroopLabeled_',num2str(indPeriod),'_*.mat']);
        load([curFile.folder, '/', curFile.name]);

        %% set trials without an answer or with incorrect answer to NaN

        removeTrials = NaN(numel(segments(4,:)),1);
        for indTrial = 1:numel(segments(4,:))
            if strcmp(segments(4,indTrial), '/')
                removeTrials(indTrial,1) = 1;
            elseif expInfo.Stroop.ColorCondsByTrial(2,:) == 1 & ~strcmp(segments(4,indTrial), 'r')
                removeTrials(indTrial,1) = 1;
            elseif expInfo.Stroop.ColorCondsByTrial(2,:) == 2 & ~strcmp(segments(4,indTrial), 'g')
                removeTrials(indTrial,1) = 1;
            elseif expInfo.Stroop.ColorCondsByTrial(2,:) == 3 & ~strcmp(segments(4,indTrial), 'b')
                removeTrials(indTrial,1) = 1;
            else
                removeTrials(indTrial,1) = 0;
            end
        end

        %% record intervals

        StroopTimes = [];
        emptyCells = find(~cellfun(@isempty,segments(7,:)'));
        segments(5, emptyCells) = segments(7, emptyCells);
        timings = segments(5,:)';
        timings(find(removeTrials)) = {[NaN, NaN]}; % set removed trials to NaN
        for indEntry = 1:size(timings,1)
            StroopTimes(indEntry,:) = cell2mat(timings(indEntry));
        end
        % calculate speech duration
        StroopTimes(:,3) = diff(StroopTimes,[],2);

%         figure;
%         imagesc(StroopTimes)

        %% split by match/mismatch & spoken word

        behavData = expInfo.Stroop.ColorCondsByTrial;

        noInterference = find(behavData(1,:)==behavData(2,:));
        interference = find(behavData(1,:)~=behavData(2,:));

        StroopInterference = NaN(81,3);
        StroopNoInterference = NaN(81,3);
        StroopInterference(1:numel(interference),:) = StroopTimes(interference,:);
        StroopNoInterference(1:numel(noInterference),:) = StroopTimes(noInterference,:);

%         figure;
%         subplot(1,2,1);
%             plot([1,2], [StroopNoInterference(:,1), StroopInterference(:,1)],'o'); xlim([0.5 2.5]);
%             title('RTs (in samples)'); xticks([1 2]); xticklabels({'Match'; 'Mismatch'});
%         subplot(1,2,2);
%             plot([1,2], [StroopNoInterference(:,3), StroopInterference(:,3)], 'o'); xlim([0.5 2.5]);
%             title('Speech duration (in samples)'); xticks([1 2]); xticklabels({'Match'; 'Mismatch'});

        Merge.StroopNoInterference(indID,indPeriod,:,:) = StroopNoInterference;
        Merge.StroopInterference(indID,indPeriod,:,:) = StroopInterference;
    end % period loop
end % ID loop

%% convert to seconds

Merge.StroopNoInterference  = Merge.StroopNoInterference./44100;
Merge.StroopInterference    = Merge.StroopInterference./44100;

%% plot group results

OnsetMatchPre_md = squeeze(nanmedian(Merge.StroopNoInterference(:,1,:,1),3));
OnsetMisMatchPre_md = squeeze(nanmedian(Merge.StroopInterference(:,1,:,1),3));
OnsetMatchPost_md = squeeze(nanmedian(Merge.StroopNoInterference(:,2,:,1),3));
OnsetMisMatchPost_md = squeeze(nanmedian(Merge.StroopInterference(:,2,:,1),3));

DurMatchPre_md = squeeze(nanmedian(Merge.StroopNoInterference(:,1,:,3),3));
DurMisMatchPre_md = squeeze(nanmedian(Merge.StroopInterference(:,1,:,3),3));
DurMatchPost_md = squeeze(nanmedian(Merge.StroopNoInterference(:,2,:,3),3));
DurMisMatchPost_md = squeeze(nanmedian(Merge.StroopInterference(:,2,:,3),3));

figure;
subplot(2,3,[1,2]);
    plot([OnsetMatchPre_md'; OnsetMisMatchPre_md'; OnsetMatchPost_md'; OnsetMisMatchPost_md'], '-o');
    xlim([.5 4.5]); set(gca, 'XTick', [1 2 3 4]);
    set(gca, 'XTickLabels', {'Match-Pre'; 'Mismatch-Pre'; 'Match-Post'; 'Mismatch-Post'});
    title('Onsets'); ylabel('Time [s]');
subplot(2,3,[4,5]);
    plot([DurMatchPre_md'; DurMisMatchPre_md'; DurMatchPost_md'; DurMisMatchPost_md'], '-o')
    xlim([.5 4.5]); set(gca, 'XTick', [1 2 3 4]);
    set(gca, 'XTickLabels', {'Match-Pre'; 'Mismatch-Pre'; 'Match-Post'; 'Mismatch-Post'});
    title('Duration'); ylabel('Time [s]');
subplot(2,3,3);
    plot([(OnsetMisMatchPre_md-OnsetMatchPre_md)'; (OnsetMisMatchPost_md-OnsetMatchPost_md)'], '-o')
    xlim([.5 2.5]); set(gca, 'XTick', [1 2]);
    set(gca, 'XTickLabels', {'StroopEffectPre'; 'StroopEffectPost'});
    title('Stroop Effect (Pre/Post): Onsets'); ylabel('Time [s]');
subplot(2,3,6);
    plot([(DurMisMatchPre_md-DurMatchPre_md)'; (DurMisMatchPost_md-DurMatchPost_md)'], '-o')
    xlim([.5 2.5]); set(gca, 'XTick', [1 2]);
    set(gca, 'XTickLabels', {'StroopEffectPre'; 'StroopEffectPost'});
    title('Stroop Effect (Pre/Post): Duration'); ylabel('Time [s]');
set(findall(gcf,'-property','FontSize'),'FontSize',15)


figure;
subplot(2,1,1);
plot([OnsetMisMatchPre_md'; OnsetMisMatchPost_md'], '-o')
subplot(2,1,2);
plot([DurMisMatchPre_md'; DurMisMatchPost_md'], '-o')

figure;
subplot(2,1,1);
plot([OnsetMatchPre_md'; OnsetMatchPost_md'], '-o')
subplot(2,1,2);
plot([DurMatchPre_md'; DurMatchPost_md'], '-o')

figure;
subplot(2,1,1);
    plot([(OnsetMisMatchPre_md-OnsetMatchPre_md)'; (OnsetMisMatchPost_md-OnsetMatchPost_md)'], '-o')
    xlim([.5 2.5]); set(gca, 'XTick', [1 2]);
    set(gca, 'XTickLabels', {'StroopEffectPre'; 'StroopEffectPost'});
    title('Stroop Effect (Pre/Post): Onsets'); ylabel('Time [s]');
subplot(2,1,2);
    plot([(DurMisMatchPre_md-DurMatchPre_md)'; (DurMisMatchPost_md-DurMatchPost_md)'], '-o')
    xlim([.5 2.5]); set(gca, 'XTick', [1 2]);
    set(gca, 'XTickLabels', {'StroopEffectPre'; 'StroopEffectPost'});
    title('Stroop Effect (Pre/Post): Duration'); ylabel('Time [s]');

figure
subplot(2,2,1);
imagesc(squeeze(nanmean(Merge.StroopNoInterference(:,1,:,1),1)), [6*10^4 9*10^4]); ylim([0 35]);
subplot(2,2,2);
imagesc(squeeze(nanmean(Merge.StroopNoInterference(:,2,:,1),1)), [6*10^4 9*10^4]); ylim([0 35]);
subplot(2,2,3);
imagesc(squeeze(nanmean(Merge.StroopInterference(:,1,:,1),1)), [6*10^4 9*10^4]); ylim([0 60]);
subplot(2,2,4);
imagesc(squeeze(nanmean(Merge.StroopInterference(:,2,:,1),1)), [6*10^4 9*10^4]); ylim([0 60]);
