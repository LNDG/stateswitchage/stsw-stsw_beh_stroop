% This is the script that is used to segment the original audio data and
% transcribe the perceived words.The detection mechanism and the audio
% player are adapted from the following two sources:
% Ressource: http://class.ee.iastate.edu/mmina/EE186/labs/Audio.htm
% Ressource: http://www.mathworks.de/de/help/signal/ref/spectrogram.html

%% setup analysis

%pn.study = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/stroop/A_CARL/';
%pn.study = '\mpib-berlin.mpg.de\FB-LIP\LNDG\Projects\StateSwitch\dynamic\data\behavior\stroop\A_CARL\'
pn.study = '/Users/kosciessa/OneDrive/WIP/A_CARL/';
pn.scripts = [pn.study, 'A_scripts/']; addpath(pn.scripts);
pn.tools = [pn.study, 'D_tools/']; addpath(pn.tools);
addpath(genpath([pn.tools, 'CARL_v170905/']))
pn.OUT = [pn.study, 'B_data/C_DataLabeled/'];

ID = '1105';
stroopVersion = '1';
pn.IN = [pn.study, 'B_data/B_DataRaw/',ID, '_StroopData_',stroopVersion, '.mat'];

%% Get dataset

load(pn.IN)

%% pre-sequence voice segments                                            
                                                                            % http://www.mathworks.com/matlabcentral/fileexchange/28826-silence-removal-in-speech-signals/content/detectVoiced.m                                                                            
% try to mark on- & offsets of the actual voice segments
segments = [];
for trial = 1:size(StroopAudio.audio,2)
    disp([num2str(trial), '/', num2str(size(StroopAudio.audio,2))]);
	segments = CARL_AutoDetectOnOffset_170905(StroopAudio.audio{1,trial}(1,:),trial, segments);
end; clear ind;

%% create GUI and transcribe words manually                                
                                                         % http://www.mathworks.de/de/help/matlab/creating_guis/about-the-simple-guide-gui-example.html
ind = []; % empty cell -> start at last empty trial
Fs = StroopAudio.s.SampleRate;
pdat = [];
info.ID = ID;
CARL_GUI_150914(segments, ind, Fs, pdat, info.ID, 'Labeling');

% Stop here and label everything; continue after labeling!

%% add timestamp and temporal distance

info.missingTimeWordonset = [];
for wordTrial = 1:size(segments,2)
    if ~isempty(segments{7,wordTrial})
        segments{8,wordTrial} = segments{7,wordTrial}(1,1) ;
        if wordTrial > 1 && ~isempty(segments{7,wordTrial-1})
            segments{9,wordTrial} = (segments{7,wordTrial}(1,1) - ...
               segments{7,wordTrial-1}(1,end))/Fs;
        end
    else info.missingTimeWordonset = [info.missingTimeWordonset, wordTrial];
    end
end

%% save

save([pn.OUT, ID , '_stroopLabeled_',stroopVersion,'_', datestr(now,'yymmdd'), '.mat'], 'segments', 'Fs');
