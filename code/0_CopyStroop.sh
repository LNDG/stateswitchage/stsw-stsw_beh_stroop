#!/bin/bash

# copy files
cp /Volumes/LNDG/Projects/StateSwitch/dynamic/raw/C_study/eeg/*/Stroop/*StroopData_1.mat /Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/stroop/A_CARL/B_data/B_DataRaw/
cp /Volumes/LNDG/Projects/StateSwitch/dynamic/raw/C_study/eeg/*/Stroop/*StroopData_2.mat /Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/stroop/A_CARL/B_data/B_DataRaw/
