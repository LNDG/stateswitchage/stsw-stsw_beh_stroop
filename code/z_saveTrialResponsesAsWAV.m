% save individual words as .wav files

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/stroop/A_CARL/';
pn.rawIn = [pn.root, 'B_data/B_DataRaw/'];
pn.wavOut = [pn.root, 'B_data/D_wavFiles/'];

%% subjects to process

% 2128 S2 missing [excluded]; 2160 S2 missing

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

%% create individual trial wave files

for indID = 1:numel(IDs)
    curID = IDs{indID};
    for indSession = 1:2
        curSes = num2str(indSession);
        % exlude data: run 2 for 2160 missing
        if strcmp(curID, '2160') && strcmp(curSes, '2')
            continue;
        end
        % create individual wave file folder
        mkdir([pn.wavOut, curID,'_', curSes]);
        % get data
        load([pn.rawIn, curID, '_StroopData_',curSes,'.mat'])
        % get trial amount
        TrialAmount = numel(StroopAudio.audio);
        % loop wave file creation across trials
        for indTrial = 1:TrialAmount
            % save as .wav
            y = StroopAudio.audio{indTrial}(1,:);
            Fs = StroopAudio.s.SampleRate;
            % change sampling rate to 16kHz
            targetFs = 16e3;
            [P,Q] = rat(targetFs/Fs);
            y_target = resample(y,P,Q);
            % save 16kHz data to disk as .wav file
            filename = [pn.wavOut, curID,'_', curSes, '/', 'Trial', num2str(indTrial,'%02d'), '.wav'];
            audiowrite(filename,y_target,targetFs)
        end
    end
end
