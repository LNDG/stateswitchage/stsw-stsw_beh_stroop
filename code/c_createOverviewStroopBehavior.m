%% get labeled data

pn.study = '/Volumes/LNDG-1/Projects/StateSwitch/dynamic/data/behavior/stroop/A_CARL/';
pn.dataFolder = [pn.study,'B_data/'];

StateSwitchDynIDs = {1117;1118;1120;1124;1125;1126;1131;1132;1135;1136;1138;1144;1151;...
    1158;1160;1163;1164;1167;1169;1172;1173;1178;1182;1213;1214;1215;1216;...
    1219;1221;1223;1227;1228;1233;1234;1237;1239;1240;1243;1245;1247;1250;...
    1252;1257;1258;1261;1265;1266;1268;1270;1276;1281;2104;2107;2108;2112;...
    2118;2120;2121;2123;2124;2125;2129;2130;2131;2132;2133;2134;2135;2139;...
    2140;2142;2145;2147;2149;2157;2160;2201;2202;2203;2205;2206;2209;2210;...
    2211;2213;2214;2215;2216;2217;2219;2222;2224;2226;2227;2236;2237;2238;...
    2241;2244;2246;2248;2250;2251;2252;2253;2254;2255;2258;2261};

StateSwitchDynIDs = cellfun(@num2str, StateSwitchDynIDs, 'un', 0);

for indID = 1:numel(StateSwitchDynIDs)
    for indPeriod = 1:2

        disp(['Processing subject ', StateSwitchDynIDs{indID},' Part ', num2str(indPeriod)]);
        
        %% load raw data with experimental structure

        rawFile = [pn.dataFolder, 'B_DataRaw/',StateSwitchDynIDs{indID},'_StroopData_',num2str(indPeriod),'.mat'];
        if isfile(rawFile)
            load(rawFile);
        else
            disp(['Data not available: ', StateSwitchDynIDs{indID}, ' Stroop ', num2str(indPeriod)]);
            data.(['StroopTarget',num2str(indPeriod)])(:,indID) = {'NaN'};
            data.(['StroopRsp',num2str(indPeriod)])(:,indID) = {'NaN'};
            data.(['StroopOnset',num2str(indPeriod)])(:,indID) = NaN;
            data.(['StroopOffset',num2str(indPeriod)])(:,indID) = NaN;
            data.(['StroopDuration',num2str(indPeriod)])(:,indID) = NaN;
            data.(['StroopInterference',num2str(indPeriod)])(:,indID) = NaN;
            continue;
        end

        %% load labeled data
        
        curFile = dir([pn.dataFolder, 'C_DataLabeled/',StateSwitchDynIDs{indID},'_stroopLabeled_',num2str(indPeriod),'_*.mat']);
        if ~isempty(curFile)
            load([curFile.folder, '/', curFile.name]);
        else
            disp(['Data not available: ', StateSwitchDynIDs{indID}, ' Stroop ', num2str(indPeriod)]);
            data.(['StroopRsp',num2str(indPeriod)])(:,indID) = {'NaN'};
            data.(['StroopOnset',num2str(indPeriod)])(:,indID) = NaN;
            data.(['StroopOffset',num2str(indPeriod)])(:,indID) = NaN;
            data.(['StroopDuration',num2str(indPeriod)])(:,indID) = NaN;
            continue;
        end

        %% encode target response
        
        data.(['StroopTarget',num2str(indPeriod)])(:,indID) = cell(size(expInfo.Stroop.ColorCondsByTrial,2),1);
        
        curTargets = []; curTargets = expInfo.Stroop.ColorCondsByTrial(2,:)';
        data.(['StroopTarget',num2str(indPeriod)])(curTargets==1,indID) = {'r'};
        data.(['StroopTarget',num2str(indPeriod)])(curTargets==2,indID) = {'g'};
        data.(['StroopTarget',num2str(indPeriod)])(curTargets==3,indID) = {'b'};
        
        
        %% remove segments that do not have response
        
        if strcmp(StateSwitchDynIDs{indID}, '1213') & indPeriod==1
            hasResponse = 1-cellfun(@isempty,segments(4,:));
            segments = segments(:,logical(hasResponse));
        end
%         if strcmp(StateSwitchDynIDs{indID}, '1234') & indPeriod==2
%             segments{4,4} = '/';
%         elseif strcmp(StateSwitchDynIDs{indID}, '1239') & indPeriod==2
%             segments{4,72} = '/';
%         end
%         
%         hasResponse = 1-cellfun(@isempty,segments(4,:));
%         segments = segments(:,logical(hasResponse));
        
        %% encode actual response

        data.(['StroopRsp',num2str(indPeriod)])(:,indID) = segments(4,:)';

        %% extract intervals from labeled data
        
        % for some reason encoded on-and offsets have not transfered to row
        % 7, copy those here
        trls2copy = cellfun(@numel,segments(5,:))==2;
        segments(7,trls2copy) = segments(5,trls2copy);
        
        StroopTimes = [];
        emptyCells = find(cellfun(@isempty,segments(7,:)')); % on- and offsets are encoded in 7th row
        timings = segments(7,:)';
        timings(emptyCells) = {[NaN, NaN]}; % set removed trials to NaN
        for indEntry = 1:size(timings,1)
            StroopTimes(indEntry,:) = cell2mat(timings(indEntry));
        end
        % calculate speech duration
        StroopTimes(:,3) = diff(StroopTimes,[],2);
        
        data.(['StroopOnset',num2str(indPeriod)])(:,indID) = StroopTimes(:,1)./44100; % convert to s
        data.(['StroopOffset',num2str(indPeriod)])(:,indID) = StroopTimes(:,2)./44100;
        data.(['StroopDuration',num2str(indPeriod)])(:,indID) = StroopTimes(:,3)./44100;

        %% split by match/mismatch & spoken word

        data.(['StroopInterference',num2str(indPeriod)])(:,indID) = ...
            double((expInfo.Stroop.ColorCondsByTrial(1,:)~=expInfo.Stroop.ColorCondsByTrial(2,:))');
        
    end % period loop
end % ID loop

%% save data

data.IDs = StateSwitchDynIDs;
save([pn.dataFolder, 'SummaryData.mat'], 'data');