% Extract behavioral trial presentation information into matrices
% Note that this does not include the accuracy information yet, that will
% have to be extracted usign some labeling procedures later.

pn.dataDir = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/stroop/B_TrialInfo/B_data/';
pn.rawDir = '/Volumes/LNDG/Projects/StateSwitch/dynamic/raw/C_study/eeg/';

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

%% copy raw data

% 2128 S2 missing [excluded]; 2160 S2 missing

mkdir([pn.dataDir, 'A_rawInput']);

copyfile([pn.rawDir, '*/Stroop/*_1.mat'], [pn.dataDir, 'A_rawInput']);
copyfile([pn.rawDir, '*/Stroop/*_2.mat'], [pn.dataDir, 'A_rawInput']);

%% get information from raw behavioral data

% Info about presented words and colors is contained in 
% expInfo.Stroop.ColorCondsByTrial
% Row 1: Presented Word, Row 2: Word Color

% Note that there are double the amount of incongruent trials (as there are two incongruent colors)

mkdir([pn.dataDir, 'B_behavioralFiles']);

for indID = 1:numel(IDs)
    for indRun = 1:2
        try
            load([pn.dataDir, 'A_rawInput/', IDs{indID}, '_StroopData_', num2str(indRun)], 'expInfo');
            behavData = [];
            behavData.presentedWord = expInfo.Stroop.ColorCondsByTrial(1,:);
            behavData.presentedColor = expInfo.Stroop.ColorCondsByTrial(2,:);
            behavData.congruency = behavData.presentedWord == behavData.presentedColor;
            save([pn.dataDir, 'B_behavioralFiles/', IDs{indID}, '_S', num2str(indRun)], 'behavData');
            clear expInfo;
        catch
            warning(['Data missing:',IDs{indID}, ' Run ', num2str(indRun),'. Please check'])
        end
    end
end

%% save as matrix