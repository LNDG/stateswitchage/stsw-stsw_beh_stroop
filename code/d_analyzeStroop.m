% path management
currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data = fullfile(rootpath, 'data');
pn.figures = fullfile(rootpath, 'figures');
pn.tools = fullfile(rootpath, 'tools'); addpath(pn.tools)
    addpath(genpath(fullfile(pn.tools, 'RainCloudPlots')));
    addpath(genpath(fullfile(pn.tools, 'ploterr')));
pn.summary = fullfile(rootpath, '..', 'stsw_multimodal', 'data');

load(fullfile(pn.data, 'SummaryData.mat'), 'data');

idxYA = str2num(cell2mat(data.IDs)) <2000;
idxOA = str2num(cell2mat(data.IDs)) >=2000;

%% descriptive statistics (missing data etc.)

N_missingTrials1 = sum(isnan(data.StroopOnset1));
N_missingTrials2 = sum(isnan(data.StroopOnset2));
missingSs_1 = find(N_missingTrials1==81);
missingSs_2 = find(N_missingTrials2==81);
Nsub_missing1 = numel(missingSs_1);
Nsub_missing2 = numel(missingSs_2);
N_missingTrials1(missingSs_1) = [];
N_missingTrials2(missingSs_2) = [];
disp(['Average missing trials S1: ', num2str(mean(N_missingTrials1)),...
    '; Average missing trials S2: ', num2str(mean(N_missingTrials2))]);

%% Individual RT reliability

color = [.8 .5 .5];

h = figure('units','centimeters','position',[.1 .1 10 10]);
set(gcf,'renderer','Painters')
hold on;
x = squeeze(nanmedian(data.StroopOnset1,1));
y = squeeze(nanmedian(data.StroopOnset2,1));
availx = ~isnan(x) & ~isnan(y); availx = availx';
scatter(x(availx), y(availx), 1, 'filled', 'MarkerFaceColor', color); 
l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
scatter(x(availx&idxYA), y(availx&idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
scatter(x(availx&idxOA), y(availx&idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color); 
x = x(availx);
y = y(availx);
%l1 = scatter(x,y, 50, [.5 .5 .5], 'filled'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
[rl1,pl1] = corrcoef(x,y);
p = pl1(2);
if p>10^-3; tit = ['p = ', num2str(round(p,2))]; else tit = sprintf('p = %.1e',p); end
legend([l1], {['r=', num2str(round(rl1(2),2)), ', ',tit]}, 'Location', 'South'); legend('boxoff');
xlabel('md SOT Session 1'); ylabel('md SOT Session 2')
title('Individual SOT reliability')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

figureName = 'D_StroopReliability';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% Age differences in median RT

% only consider matching trials, mean across both sessions

for indID = 1:size(data.StroopOnset1,2)
    data.StroopOnsetMatch1(:,indID) = nanmedian(data.StroopOnset1(data.StroopInterference1(:,indID)==0,indID),1);
    data.StroopOnsetMatch2(:,indID) = nanmedian(data.StroopOnset2(data.StroopInterference2(:,indID)==0,indID),1);
    data.StroopOnsetMismatch1(:,indID) = nanmedian(data.StroopOnset1(data.StroopInterference1(:,indID)==1,indID),1);
    data.StroopOnsetMismatch2(:,indID) = nanmedian(data.StroopOnset2(data.StroopInterference2(:,indID)==1,indID),1);
end

plot_data{1} = squeeze(nanmean(cat(1,data.StroopOnsetMatch1(:,idxYA),data.StroopOnsetMatch2(:,idxYA)),1))';
plot_data{2} = squeeze(nanmean(cat(1,data.StroopOnsetMatch1(:,idxOA),data.StroopOnsetMatch2(:,idxOA)),1))';

h = figure('units','centimeters','position',[.1 .1 10 10]);
set(gcf,'renderer','Painters')
cla;
hold on;
for indGroup = 1:2
    bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor',  [.7 .7 .7], 'EdgeColor', 'none', 'BarWidth', 0.8);
    % show standard error on top
%     h1 = ploterr(indGroup, nanmean(plot_data{indGroup},1), [], ...
%         nanstd(plot_data{indGroup},[],1)./sqrt(size(plot_data{indGroup},1)), 'r');
%     set(h1(1), 'marker', 'none'); % remove marker
% 	set(h1(2), 'LineWidth', 20);
    % plot individual values on top
    scatter(repmat(indGroup,numel(plot_data{indGroup}),1)+(rand(numel(plot_data{indGroup}),1)-.5).*.4,...
        plot_data{indGroup}, 30, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
end
xlim([.25 2.75]); ylim([1.3 2])
set(gca,'xtick',[1,2]); set(gca,'xTickLabel',{'YA'; 'OA'}); xlabel('Age Group'); ylabel({'md Onset time (RT)'})
set(findall(gcf,'-property','FontSize'),'FontSize',20)

[h, p] = ttest2(plot_data{1}, plot_data{2});
text(.5, 1.95, ['p = ', num2str(p)])

figureName = 'D_RTbyAge';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% RT interference costs

data.StroopOnset = cat(1, data.StroopOnset1, data.StroopOnset2);
data.StroopInterference = cat(1, data.StroopInterference1, data.StroopInterference2);

for indID = 1:size(data.StroopInterference1,2)
    Interference0(indID) = squeeze(nanmean(data.StroopOnset(data.StroopInterference(:,indID)==0,indID),1));
    Interference1(indID) = squeeze(nanmean(data.StroopOnset(data.StroopInterference(:,indID)==1,indID),1));
end
InterferenceCosts = (Interference1-Interference0)./Interference0;

%% plot interference effect across age groups
% 
% plot_data{1} = Interference0';
% plot_data{2} = Interference1';
% 
% h = figure('units','centimeters','position',[.1 .1 10 10]);
% set(gcf,'renderer','Painters')
% cla;
% hold on;
% for indGroup = 1:2
%     bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor',  [.7 .7 .7], 'EdgeColor', 'none', 'BarWidth', 0.8);
%     % show standard error on top
%     h1 = ploterr(indGroup, nanmean(plot_data{indGroup},1), [], ...
%         nanstd(plot_data{indGroup},[],1)./sqrt(size(plot_data{indGroup},1)), 'r');
%     set(h1(1), 'marker', 'none'); % remove marker
% 	set(h1(2), 'LineWidth', 20);
%     % plot individual values on top
%     plot([Interference0', Interference1']', 'Color', [.3 .3 .3])
% %     scatter(repmat(indGroup,numel(plot_data{indGroup}),1)+(rand(numel(plot_data{indGroup}),1)-.5).*.4,...
% %         plot_data{indGroup}, 20, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
% end
% xlim([.25 2.75]); ylim([1.3 2.3])
% set(gca,'xtick',[1,2]); set(gca,'xTickLabel',{'No'; 'Yes'}); xlabel('Interference'); ylabel({'md Onset time (RT)'})
% set(findall(gcf,'-property','FontSize'),'FontSize',20)
% 
% [tmp, p] = ttest2(plot_data{1}, plot_data{2});%p
% text(.5, 2.2, ['p = ', num2str(p)])

%% interference costs by age group

plot_data{1} = InterferenceCosts(idxYA)';
plot_data{2} = InterferenceCosts(idxOA)';

h = figure('units','centimeters','position',[.1 .1 8 10]);
set(gcf,'renderer','Painters')
cla;
hold on;
for indGroup = 1:2
    bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor',  [.7 .7 .7], 'EdgeColor', 'none', 'BarWidth', 0.8);
    % show standard error on top
%     h1 = ploterr(indGroup, nanmean(plot_data{indGroup},1), [], ...
%         nanstd(plot_data{indGroup},[],1)./sqrt(size(plot_data{indGroup},1)), 'r');
%     set(h1(1), 'marker', 'none'); % remove marker
% 	set(h1(2), 'LineWidth', 20);
    % plot individual values on top
    scatter(repmat(indGroup,numel(plot_data{indGroup}),1)+(rand(numel(plot_data{indGroup}),1)-.5).*.4,...
        plot_data{indGroup}, 30, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
end
xlim([.25 2.75]); %ylim([.95 1.25])
set(gca,'xtick',[1,2]); set(gca,'xTickLabel',{'YA'; 'OA'}); xlabel('Age Group'); ylabel({'Interference Costs (SOT)'})
set(findall(gcf,'-property','FontSize'),'FontSize',20)

[tmp, p] = ttest2(plot_data{1}, plot_data{2});%p
title(['p = ', num2str(p)])

figureName = 'D_InterferencebyAge';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

% within-group statistics

for indGroup = 1:2
    plot_data{indGroup}(isnan(plot_data{indGroup})) = [];
    [h_test,p,ci,stats] = ttest(plot_data{indGroup});
    disp(['Group ', num2str(indGroup), ...
        ', df ', num2str(stats.df), ...
        ', t-value: ', num2str(round(stats.tstat,2)), ...
        ', p-value: ', sprintf('%.1e',p), ...
        ', cohen-d: ', num2str(round(mean(plot_data{indGroup})/std(plot_data{indGroup}),2))]);
end

%% interference costs by age group and session

for indID = 1:size(data.StroopInterference1,2)
    InterferenceS1_0(indID) = squeeze(nanmedian(data.StroopOnset1(data.StroopInterference1(:,indID)==0,indID),1));
    InterferenceS1_1(indID) = squeeze(nanmedian(data.StroopOnset1(data.StroopInterference1(:,indID)==1,indID),1));
    InterferenceS2_0(indID) = squeeze(nanmedian(data.StroopOnset2(data.StroopInterference2(:,indID)==0,indID),1));
    InterferenceS2_1(indID) = squeeze(nanmedian(data.StroopOnset2(data.StroopInterference2(:,indID)==1,indID),1));
end
InterferenceCostsS1 = (InterferenceS1_1-InterferenceS1_0)./InterferenceS1_0;
InterferenceCostsS2 = (InterferenceS2_1-InterferenceS2_0)./InterferenceS2_0;

plot_data{1} = InterferenceCostsS1(idxYA)';
plot_data{2} = InterferenceCostsS1(idxOA)';
plot_data{3} = InterferenceCostsS2(idxYA)';
plot_data{4} = InterferenceCostsS2(idxOA)';

h = figure('units','normalized','position',[.1 .1 .15 .2]);
set(gcf,'renderer','Painters')
cla;
hold on;
for indGroup = 1:4
    bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor',  [.7 .7 .7], 'EdgeColor', 'none', 'BarWidth', 0.8);
    % show standard error on top
%     h1 = ploterr(indGroup, nanmean(plot_data{indGroup},1), [], ...
%         nanstd(plot_data{indGroup},[],1)./sqrt(size(plot_data{indGroup},1)), 'r');
%     set(h1(1), 'marker', 'none'); % remove marker
% 	set(h1(2), 'LineWidth', 20);
    % plot individual values on top
    scatter(repmat(indGroup,numel(plot_data{indGroup}),1)+(rand(numel(plot_data{indGroup}),1)-.5).*.4,...
        plot_data{indGroup}, 20, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
end
xlim([.25 4.75]); %ylim([1.3 2])
set(gca,'xtick',[1,2,3,4]); set(gca,'xTickLabel',{'YA'; 'OA'; 'YA'; 'OA'}); xlabel('Age Group'); ylabel({'Interference Costs (RT)'})
set(findall(gcf,'-property','FontSize'),'FontSize',20)

[tmp, p] = ttest2(plot_data{1}, plot_data{3});%p
text(.5, .25, ['p = ', num2str(p)])

[tmp, p] = ttest2(plot_data{2}, plot_data{4});%p
text(2.5, .25, ['p = ', num2str(p)])

% No session differences in interference effect on RT

figureName = 'D_InterferencebyAgePrePost';
saveas(h, fullfile(pn.figures, figureName), 'png');

%% Reliability of individual interference costs

color = [.8 .5 .5];

h = figure('units','centimeters','position',[.1 .1 10 10]);
set(gcf,'renderer','Painters')
hold on;
x = squeeze(nanmean(InterferenceCostsS1,1));
y = squeeze(nanmean(InterferenceCostsS2,1));
availx = ~isnan(x) & ~isnan(y); availx = availx';
scatter(x(availx), y(availx), 1, 'filled', 'MarkerFaceColor', color); 
l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
scatter(x(availx&idxYA), y(availx&idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
scatter(x(availx&idxOA), y(availx&idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color); 
x = x(availx);
y = y(availx);
%l1 = scatter(x,y, 50, [.5 .5 .5], 'filled'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
[rl1,pl1] = corrcoef(x,y);
p = pl1(2);
if p>10^-3; tit = ['p = ', num2str(round(p,2))]; else tit = sprintf('p = %.1e',p); end
legend([l1], {['r=', num2str(round(rl1(2),2)), ', ',tit]}, 'Location', 'North'); legend('boxoff');
xlabel('Interference Session 1'); ylabel('Interference Session 2')
title('Individual interference reliability')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
figureName = 'D_StroopReliability_Interference';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% consider accuracy

data.acc1 = double(cellfun(@strcmp, data.StroopTarget1, data.StroopRsp1));
data.acc2 = double(cellfun(@strcmp, data.StroopTarget2, data.StroopRsp2));

% retain missing data indication

miss1 = cellfun(@strcmp, data.StroopRsp1, repmat({'NaN'},size(data.StroopRsp1,1),size(data.StroopRsp1,2)));
miss2 = cellfun(@strcmp, data.StroopRsp2, repmat({'NaN'},size(data.StroopRsp1,1),size(data.StroopRsp1,2)));

data.acc1(miss1) = NaN;
data.acc2(miss2) = NaN;

%% interference costs

for indID = 1:size(data.StroopInterference1,2)
    Interference0(indID) = squeeze(nanmean(data.acc1(data.StroopInterference1(:,indID)==0,indID),1));
    Interference1(indID) = squeeze(nanmean(data.acc2(data.StroopInterference1(:,indID)==1,indID),1));
end
InterferenceCosts = (Interference1-Interference0)./Interference0;

%% plot interference effect across age groups

plot_data{1} = Interference0';
plot_data{2} = Interference1';

h = figure('units','normalized','position',[.1 .1 .15 .2]);
set(gcf,'renderer','Painters')
cla;
hold on;
for indGroup = 1:2
    bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor',  [.7 .7 .7], 'EdgeColor', 'none', 'BarWidth', 0.8);
    % show standard error on top
%     h1 = ploterr(indGroup, nanmean(plot_data{indGroup},1), [], ...
%         nanstd(plot_data{indGroup},[],1)./sqrt(size(plot_data{indGroup},1)), 'r');
%     set(h1(1), 'marker', 'none'); % remove marker
% 	set(h1(2), 'LineWidth', 20);
    % plot individual values on top
    plot([Interference0', Interference1']', 'Color', [.3 .3 .3])
%     scatter(repmat(indGroup,numel(plot_data{indGroup}),1)+(rand(numel(plot_data{indGroup}),1)-.5).*.4,...
%         plot_data{indGroup}, 20, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
end
xlim([.25 2.75]); ylim([0.5 1])
set(gca,'xtick',[1,2]); set(gca,'xTickLabel',{'No'; 'Yes'}); xlabel('Interference'); ylabel({'Accuracy'})
set(findall(gcf,'-property','FontSize'),'FontSize',20)

[tmp, p] = ttest2(plot_data{1}, plot_data{2});%p
text(.5, .6, ['p = ', num2str(p)])

figureName = 'D_AccbyInterference';
saveas(h, fullfile(pn.figures, figureName), 'png');

%% interference costs by age group

plot_data{1} = InterferenceCosts(idxYA)';
plot_data{2} = InterferenceCosts(idxOA)';

h = figure('units','normalized','position',[.1 .1 .15 .2]);
set(gcf,'renderer','Painters')
cla;
hold on;
for indGroup = 1:2
    bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor',  [.7 .7 .7], 'EdgeColor', 'none', 'BarWidth', 0.8);
    % show standard error on top
%     h1 = ploterr(indGroup, nanmean(plot_data{indGroup},1), [], ...
%         nanstd(plot_data{indGroup},[],1)./sqrt(size(plot_data{indGroup},1)), 'r');
%     set(h1(1), 'marker', 'none'); % remove marker
% 	set(h1(2), 'LineWidth', 20);
    % plot individual values on top
    scatter(repmat(indGroup,numel(plot_data{indGroup}),1)+(rand(numel(plot_data{indGroup}),1)-.5).*.4,...
        plot_data{indGroup}, 20, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
end
xlim([.25 2.75]); %ylim([1.3 2])
set(gca,'xtick',[1,2]); set(gca,'xTickLabel',{'YA'; 'OA'}); xlabel('Age Group'); ylabel({'Interference Costs (accuracy)'})
set(findall(gcf,'-property','FontSize'),'FontSize',20)

[tmp, p] = ttest2(plot_data{1}, plot_data{2});%p
text(.5, .05, ['p = ', num2str(p)])

% Accuracy is basically at ceiling across conditions, no interference/group effects
% This would be expected in a Stroop task, but maybe there are
% counterexamples/meta-analyses that argue otherwise?

% This may actually well motivate a DDM7CPP approach given thataccuracy is
% at ceiling.

%% RT interference costs

data.StroopOnset = cat(1, data.StroopOnset1, data.StroopOnset2);
data.StroopInterference = cat(1, data.StroopInterference1, data.StroopInterference2);

for indID = 1:size(data.StroopInterference1,2)
    Interference0(indID) = squeeze(nanmedian(data.StroopOnset(data.StroopInterference(:,indID)==0,indID),1));
    Interference1(indID) = squeeze(nanmedian(data.StroopOnset(data.StroopInterference(:,indID)==1,indID),1));
end
InterferenceCosts = (Interference1-Interference0)./Interference0;

% save stroop estimates

save(fullfile(pn.data, 'stroop_params.mat'),...
    'InterferenceCosts', 'Interference0', 'Interference1');

%% correlations with drift rate

load(fullfile(pn.summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')

% N = 42 YA, 1125, 1214 excluded: no EEG data, 1138, 1144, 1158, 1163, 1221 no MR
IDs = {'1117';'1118';'1120';'1124'; '1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182'; '1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

idxYA = ismember(STSWD_summary.IDs, IDs);

RT = cat(1,nanmean(data.StroopOnset1(:,idxYA),1),nanmean(data.StroopOnset2(:,idxYA),1));
drift = cat(2,STSWD_summary.HDDM_vat.driftEEGMRI(idxYA,1),STSWD_summary.HDDM_vat.driftMRI(idxYA,1));

x = squeeze(nanmean(RT,1))';
y = squeeze(nanmean(drift,2)); %[x,y]
figure; scatter(x(~isnan(x)),y(~isnan(x)), 'filled')
[r, p] = corrcoef(x(~isnan(x)),y(~isnan(x)))

RT = cat(1,nanmean(data.StroopOnset1(:,idxOA),1),nanmean(data.StroopOnset2(:,idxOA),1));
drift = cat(2,STSWD_summary.HDDM_vat.driftEEGMRI(idxOA,1),STSWD_summary.HDDM_vat.driftMRI(idxOA,1));

x = squeeze(nanmean(RT,1));
y = squeeze(nanmean(drift,2));
figure; scatter(x(~isnan(x)),y(~isnan(x)), 'filled')
[r, p] = corrcoef(x(~isnan(x)),y(~isnan(x)))

% both groups
RT = cat(1,nanmean(data.StroopOnset1(:,logical(idxOA+idxYA)),1),nanmean(data.StroopOnset2(:,logical(idxOA+idxYA)),1));
drift = cat(2,STSWD_summary.HDDM_vat.driftEEGMRI(logical(idxOA+idxYA),1),STSWD_summary.HDDM_vat.driftMRI(logical(idxOA+idxYA),1));

% x = squeeze(nanmean(RT,1))';
% y = squeeze(nanmean(drift,2));
% figure; scatter(x(~isnan(x)),y(~isnan(x)), 50, [.5 .5 .5], 'filled')
% [r, p] = corrcoef(x(~isnan(x)),y(~isnan(x)))
% xlabel('RT Stroop'); ylabel('Drift Rate L1 MAAT')
% set(findall(gcf,'-property','FontSize'),'FontSize',20)

available = ~isnan(squeeze(nanmedian(data.StroopOnset1,1)));
x = squeeze(nanmean(RT,1))';
availx = ~isnan(x);
x = x(availx);
y = squeeze(nanmean(drift,2));
y = y(availx);

h = figure; 
hold on;
l1 = scatter(x,y, 50, [.5 .5 .5], 'filled'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
[rl1,pl1] = corrcoef(x,y); pval1 = []; pval1 = convertPtoExponential(pl1(2));
% control for age
X = [x,y,cat(1,repmat(1,numel(find(idxYA'.*available)),1),repmat(2,numel(find(idxOA'.*available)),1))];
[rho_control, pval_control] = partialcorr(X)
pval_control = convertPtoExponential(pval_control(1,2));
legend([y1_ls], {['All: r=', num2str(round(rl1(2),2)), ', p=',pval1{1}, ...
    ', control r=', num2str(round(rho_control(1,2),2)), ', p=',pval_control{1}]}, 'Location', 'NorthEast'); legend('boxoff');
xlabel('Stroop RT'); ylabel('Drift Rate L1 MAAT')
set(findall(gcf,'-property','FontSize'),'FontSize',20)

figureName = 'D_StroopRT_Drift';
saveas(h, fullfile(pn.figures, figureName), 'png');
saveas(h, fullfile(pn.figures, figureName), 'eps');

% relation also holds when controlling for categorical age (p = .02)

%% same as above, but only consider match trials now

color = [.5 .6 .8];

h = figure('units','centimeters','position',[.1 .1 10 10]);
set(gcf,'renderer','Painters')
hold on;
available = ~isnan(squeeze(nanmean(data.StroopOnsetMatch1,1)));
RT = Interference0;
x = squeeze(nanmedian(RT,1))';
drift = STSWD_summary.HDDM_vat.driftEEGMRI(:,1);
y = squeeze(nanmean(drift,2));
availx = ~isnan(x) & ~isnan(y);
scatter(x(availx), y(availx), 1, 'filled', 'MarkerFaceColor', color); 
l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
scatter(x(availx&idxYA), y(availx&idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
scatter(x(availx&idxOA), y(availx&idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color); 
x = x(availx&idxYA+idxOA);
y = y(availx&idxYA+idxOA);
%l1 = scatter(x,y, 50, [.5 .5 .5], 'filled'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
[rl1,pl1] = corrcoef(x,y);
p = pl1(2);
if p>10^-3; tit = ['p = ', num2str(round(p,2))]; else tit = sprintf('p = %.1e',p); end
% control for age
X = [x,y,cat(1,repmat(1,numel(find(idxYA.*availx)),1),repmat(2,numel(find(idxOA.*availx)),1))];
[rho_control, pval_control] = partialcorr(X);
p_control = pval_control(2);
if p_control>10^-3; tit_c = ['p = ', num2str(round(p_control,2))]; else tit_c = sprintf('p = %.1e',p_control); end
legend([l1], {['r=', num2str(round(rl1(2),2)), ', ',tit, ...
    ', control r=', num2str(round(rho_control(1,2),2)), ', ',tit_c]}, 'Location', 'NorthEast');
legend('boxoff');
xlabel('Stroop SOT Match'); ylabel('Drift Rate L1 MAAT')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

% Relation does not hold when considering joint age differences (p = .14)
% This may suggest that MAAT drift rate differences do not capture pure
% response speed, but rather be selective interference costs

figureName = 'D_StroopRTmatch_Drift';
saveas(h, fullfile(pn.figures, figureName), 'png');

%% plot exclusively for mismatch trials

color = [.5 .6 .8];

h = figure('units','centimeters','position',[.1 .1 10 10]);
set(gcf,'renderer','Painters')
hold on;
available = ~isnan(squeeze(nanmean(data.StroopOnsetMatch1,1)));
RT = Interference1;
x = squeeze(nanmedian(RT,1))';
drift = STSWD_summary.HDDM_vat.driftEEGMRI(:,1);
y = squeeze(nanmean(drift,2));
availx = ~isnan(x) & ~isnan(y);
scatter(x(availx), y(availx), 1, 'filled', 'MarkerFaceColor', color); 
l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
scatter(x(availx&idxYA), y(availx&idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
scatter(x(availx&idxOA), y(availx&idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color); 
x = x(availx&idxYA+idxOA);
y = y(availx&idxYA+idxOA);
%l1 = scatter(x,y, 50, [.5 .5 .5], 'filled'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
[rl1,pl1] = corrcoef(x,y);
p = pl1(2);
if p>10^-3; tit = ['p = ', num2str(round(p,2))]; else tit = sprintf('p = %.1e',p); end
% control for age
X = [x,y,cat(1,repmat(1,numel(find(idxYA.*availx)),1),repmat(2,numel(find(idxOA.*availx)),1))];
[rho_control, pval_control] = partialcorr(X);
p_control = pval_control(2);
if p_control>10^-3; tit_c = ['p = ', num2str(round(p_control,2))]; else tit_c = sprintf('p = %.1e',p_control); end
legend([l1], {['r=', num2str(round(rl1(2),2)), ', ',tit, ...
    ', control r=', num2str(round(rho_control(1,2),2)), ', ',tit_c]}, 'Location', 'NorthEast');
legend('boxoff');
xlabel('Stroop SOT Mismatch'); ylabel('Drift Rate L1 MAAT')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

% In contrast, we observe a significant relation between MAAT drift rates
% and SOTs in mismatch trials, suggetsing that the interference effect is
% in fact crucial for the drift rate relation, rather than processing speed.

figureName = 'D_StroopRTmismatch_Drift';
saveas(h, fullfile(pn.figures, figureName), 'png');

%% assess relation of drift rate to individual RT interference costs

color = [.5 .6 .8];

h = figure('units','centimeters','position',[.1 .1 10 10]);
set(gcf,'renderer','Painters')
hold on;
available = ~isnan(squeeze(nanmean(data.StroopOnsetMatch1,1)));
RT = InterferenceCosts;
x = squeeze(nanmedian(RT,1))';
drift = STSWD_summary.HDDM_vat.driftEEGMRI(:,1);
y = squeeze(nanmean(drift,2));
availx = ~isnan(x) & ~isnan(y);
scatter(x(availx), y(availx), 1, 'filled', 'MarkerFaceColor', color); 
l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
scatter(x(availx&idxYA), y(availx&idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
scatter(x(availx&idxOA), y(availx&idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color); 
x = x(availx&idxYA+idxOA);
y = y(availx&idxYA+idxOA);
%l1 = scatter(x,y, 50, [.5 .5 .5], 'filled'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
[rl1,pl1] = corrcoef(x,y);
p = pl1(2);
if p>10^-3; tit = ['p = ', num2str(round(p,2))]; else tit = sprintf('p = %.1e',p); end
% control for age
X = [x,y,cat(1,repmat(1,numel(find(idxYA.*availx)),1),repmat(2,numel(find(idxOA.*availx)),1))];
[rho_control, pval_control] = partialcorr(X);
p_control = pval_control(2);
if p_control>10^-3; tit_c = ['p = ', num2str(round(p_control,2))]; else tit_c = sprintf('p = %.1e',p_control); end
legend([l1], {['r=', num2str(round(rl1(2),2)), ', ',tit, ...
    ', control r=', num2str(round(rho_control(1,2),2)), ', ',tit_c]}, 'Location', 'NorthEast');
legend('boxoff');
xlabel('Stroop SOT Interference'); ylabel('Drift Rate L1 MAAT')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

figureName = 'D_Interference_Drift';
saveas(h, fullfile(pn.figures, figureName), 'png');
saveas(h, fullfile(pn.figures, figureName), 'eps');

figureName = 'Fig2c_StroopInter_Drift_correlation';
saveas(h, fullfile(pn.figures, figureName), 'eps');

%% plot load-related changes in drift rate
% 
% interference = InterferenceCosts(logical(idxOA+idxYA));
% drift = cat(2,STSWD_summary.HDDM_vat.driftEEG_linear(logical(idxOA+idxYA),1),STSWD_summary.HDDM_vat.driftMRI_linear(logical(idxOA+idxYA),1));
% 
% available = ~isnan(InterferenceCosts);
% x = interference';
% availx = ~isnan(x);
% x = x(availx);
% y = squeeze(nanmean(drift,2));
% y = y(availx);
% 
% h = figure; 
% hold on;
% l1 = scatter(x,y, 50, [.5 .5 .5], 'filled'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
% [rl1,pl1] = corrcoef(x,y); pval1 = []; pval1 = convertPtoExponential(pl1(2));
% % control for age
% X = [x,y,cat(1,repmat(1,numel(find(idxYA'.*available)),1),repmat(2,numel(find(idxOA'.*available)),1))];
% [rho_control, pval_control] = partialcorr(X)
% pval_control = convertPtoExponential(pval_control(1,2));
% legend([y1_ls], {['All: r=', num2str(round(rl1(2),2)), ', p=',pval1{1}, ...
%     ', control r=', num2str(round(rho_control(1,2),2)), ', p=',pval_control{1}]}, 'Location', 'NorthEast'); legend('boxoff');
% xlabel('Stroop Interference'); ylabel('Drift Rate Linear changes MAAT')
% set(findall(gcf,'-property','FontSize'),'FontSize',20)
% % Relation also holds when considering joint age differences (p = .02)
% 
% figureName = 'D_Interference_DriftLinear';
% saveas(h, fullfile(pn.figures, figureName), 'png');
% saveas(h, fullfile(pn.figures, figureName), 'eps');

%% relate session specific

h = figure;
subplot(2,2,1)
    hold on;
    interference = InterferenceCostsS1(logical(idxOA+idxYA));
    drift = cat(2,STSWD_summary.HDDM_vat.driftEEGMRI(logical(idxOA+idxYA),1));
    available = ~isnan(InterferenceCosts);
    x = interference';
    availx = ~isnan(x);
    x = x(availx);
    y = squeeze(nanmean(drift,2));
    y = y(availx);
    hold on;
    l1 = scatter(x,y, 50, [.5 .5 .5], 'filled'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
    [rl1,pl1] = corrcoef(x,y); pval1 = []; pval1 = convertPtoExponential(pl1(2));
    % control for age
    X = [x,y,cat(1,repmat(1,numel(find(idxYA'.*available)),1),repmat(2,numel(find(idxOA'.*available)),1))];
    [rho_control, pval_control] = partialcorr(X)
    pval_control = convertPtoExponential(pval_control(1,2));
    legend([y1_ls], {['All: r=', num2str(round(rl1(2),2)), ', p=',pval1{1}, ...
        ', control r=', num2str(round(rho_control(1,2),2)), ', p=',pval_control{1}]}, 'Location', 'NorthEast'); legend('boxoff');
    xlabel('Stroop Interference S1'); ylabel('Drift Rate L1 MAAT EEG')
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
subplot(2,2,2)
    hold on;
    interference = InterferenceCostsS2(logical(idxOA+idxYA));
    drift = cat(2,STSWD_summary.HDDM_vat.driftEEGMRI(logical(idxOA+idxYA),1));
    available = ~isnan(InterferenceCostsS2);
    x = interference';
    availx = ~isnan(x);
    x = x(availx);
    y = squeeze(nanmean(drift,2));
    y = y(availx);
    hold on;
    l1 = scatter(x,y, 50, [.5 .5 .5], 'filled'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
    [rl1,pl1] = corrcoef(x,y); pval1 = []; pval1 = convertPtoExponential(pl1(2));
    % control for age
    X = [x,y,cat(1,repmat(1,numel(find(idxYA'.*available)),1),repmat(2,numel(find(idxOA'.*available)),1))];
    [rho_control, pval_control] = partialcorr(X)
    pval_control = convertPtoExponential(pval_control(1,2));
    legend([y1_ls], {['All: r=', num2str(round(rl1(2),2)), ', p=',pval1{1}, ...
        ', control r=', num2str(round(rho_control(1,2),2)), ', p=',pval_control{1}]}, 'Location', 'NorthEast'); legend('boxoff');
    xlabel('Stroop Interference S2'); ylabel('Drift Rate L1 MAAT EEG')
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
subplot(2,2,3)
    hold on;
    interference = InterferenceCostsS1(logical(idxOA+idxYA));
    drift = cat(2,STSWD_summary.HDDM_vat.driftMRI(logical(idxOA+idxYA),1));
    available = ~isnan(InterferenceCosts);
    x = interference';
    availx = ~isnan(x);
    x = x(availx);
    y = squeeze(nanmean(drift,2));
    y = y(availx);
    hold on;
    l1 = scatter(x,y, 50, [.5 .5 .5], 'filled'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
    [rl1,pl1] = corrcoef(x,y); pval1 = []; pval1 = convertPtoExponential(pl1(2));
    % control for age
    X = [x,y,cat(1,repmat(1,numel(find(idxYA'.*available)),1),repmat(2,numel(find(idxOA'.*available)),1))];
    [rho_control, pval_control] = partialcorr(X)
    pval_control = convertPtoExponential(pval_control(1,2));
    legend([y1_ls], {['All: r=', num2str(round(rl1(2),2)), ', p=',pval1{1}, ...
        ', control r=', num2str(round(rho_control(1,2),2)), ', p=',pval_control{1}]}, 'Location', 'NorthEast'); legend('boxoff');
    xlabel('Stroop Interference S1'); ylabel('Drift Rate L1 MAAT MRI')
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
subplot(2,2,4)
    hold on;
    interference = InterferenceCostsS2(logical(idxOA+idxYA));
    drift = cat(2,STSWD_summary.HDDM_vat.driftMRI(logical(idxOA+idxYA),1));
    available = ~isnan(InterferenceCostsS2);
    x = interference';
    availx = ~isnan(x);
    x = x(availx);
    y = squeeze(nanmean(drift,2));
    y = y(availx);
    hold on;
    l1 = scatter(x,y, 50, [.5 .5 .5], 'filled'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
    [rl1,pl1] = corrcoef(x,y); pval1 = []; pval1 = convertPtoExponential(pl1(2));
    % control for age
    X = [x,y,cat(1,repmat(1,numel(find(idxYA'.*available)),1),repmat(2,numel(find(idxOA'.*available)),1))];
    [rho_control, pval_control] = partialcorr(X)
    pval_control = convertPtoExponential(pval_control(1,2));
    legend([y1_ls], {['All: r=', num2str(round(rl1(2),2)), ', p=',pval1{1}, ...
        ', control r=', num2str(round(rho_control(1,2),2)), ', p=',pval_control{1}]}, 'Location', 'NorthEast'); legend('boxoff');
    xlabel('Stroop Interference S2'); ylabel('Drift Rate L1 MAAT MRI')
    set(findall(gcf,'-property','FontSize'),'FontSize',20)


%% relation between general RT and interference costs

interference = InterferenceCosts(logical(idxOA+idxYA));
RT = cat(1,nanmean(data.StroopOnsetMatch1(:,logical(idxOA+idxYA)),1),nanmean(data.StroopOnsetMatch2(:,logical(idxOA+idxYA)),1));

available = ~isnan(InterferenceCosts);
x = interference';
availx = ~isnan(x);
x = x(availx);
y = squeeze(nanmean(RT,1))';
y = y(availx);

h = figure; 
hold on;
l1 = scatter(x,y, 50, [.5 .5 .5], 'filled'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
[rl1,pl1] = corrcoef(x,y); pval1 = []; pval1 = convertPtoExponential(pl1(2));
% control for age
X = [x,y,cat(1,repmat(1,numel(find(idxYA'.*available)),1),repmat(2,numel(find(idxOA'.*available)),1))];
[rho_control, pval_control] = partialcorr(X)
pval_control = convertPtoExponential(pval_control(1,2));
legend([y1_ls], {['All: r=', num2str(round(rl1(2),2)), ', p=',pval1{1}, ...
    ', control r=', num2str(round(rho_control(1,2),2)), ', p=',pval_control{1}]}, 'Location', 'NorthEast'); legend('boxoff');
xlabel('Stroop Interference'); ylabel('Stroop RT')
set(findall(gcf,'-property','FontSize'),'FontSize',20)

figureName = 'D_Interference_RT';
saveas(h, fullfile(pn.figures, figureName), 'png');

%% relation between drift mod. and Stroop performance

h = figure; 
subplot(1,2,1);
    RT = cat(1,nanmean(data.StroopOnsetMatch1(:,logical(idxOA+idxYA)),1),nanmean(data.StroopOnsetMatch2(:,logical(idxOA+idxYA)),1));
    drift = nanmean(cat(2,STSWD_summary.HDDM_vat.driftEEG_linear(logical(idxOA+idxYA),1),STSWD_summary.HDDM_vat.driftMRI_linear(logical(idxOA+idxYA),1)),2);

    available = ~isnan(squeeze(nanmean(data.StroopOnset1,1)));
    x = squeeze(nanmean(RT,1))';
    availx = ~isnan(x);
    x = x(availx);
    y = squeeze(nanmean(drift,2));
    y = y(availx);

    hold on;
    l1 = scatter(x,y, 50, [.5 .5 .5], 'filled'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
    [rl1,pl1] = corrcoef(x,y); pval1 = []; pval1 = convertPtoExponential(pl1(2));
    % control for age
    X = [x,y,cat(1,repmat(1,numel(find(idxYA'.*available)),1),repmat(2,numel(find(idxOA'.*available)),1))];
    [rho_control, pval_control] = partialcorr(X)
    pval_control = convertPtoExponential(pval_control(1,2));
    legend([y1_ls], {['All: r=', num2str(round(rl1(2),2)), ', p=',pval1{1}, ...
        ', control r=', num2str(round(rho_control(1,2),2)), ', p=',pval_control{1}]}, 'Location', 'NorthEast'); legend('boxoff');
    xlabel('Stroop RT'); ylabel('MAAT: linear drift')
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
subplot(1,2,2);
    interference = InterferenceCosts(logical(idxOA+idxYA));
    drift = nanmean(cat(2,STSWD_summary.HDDM_vat.driftEEG_linear(logical(idxOA+idxYA),1),STSWD_summary.HDDM_vat.driftMRI_linear(logical(idxOA+idxYA),1)),2);

    available = ~isnan(InterferenceCosts);
    x = interference';
    availx = ~isnan(x);
    x = x(availx);
    y = squeeze(nanmean(drift,2));
    y = y(availx);

    hold on;
    l1 = scatter(x,y, 50, [.5 .5 .5], 'filled'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
    [rl1,pl1] = corrcoef(x,y); pval1 = []; pval1 = convertPtoExponential(pl1(2));
    % control for age
    X = [x,y,cat(1,repmat(1,numel(find(idxYA'.*available)),1),repmat(2,numel(find(idxOA'.*available)),1))];
    [rho_control, pval_control] = partialcorr(X)
    pval_control = convertPtoExponential(pval_control(1,2));
    legend([y1_ls], {['All: r=', num2str(round(rl1(2),2)), ', p=',pval1{1}, ...
        ', control r=', num2str(round(rho_control(1,2),2)), ', p=',pval_control{1}]}, 'Location', 'NorthEast'); legend('boxoff');
    xlabel('Stroop Interference'); ylabel('MAAT: linear drift')
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    
figureName = 'D_linearDrift_Stroop';
saveas(h, fullfile(pn.figures, figureName), 'png');