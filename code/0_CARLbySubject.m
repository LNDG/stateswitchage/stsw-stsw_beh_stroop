pn.root = '/Volumes/LNDG/stateSwitch/D_analyses/A_Stroop/';
pn.scripts = [pn.root, 'A_Scripts/']; addpath(pn.scripts);
pn.tools = [pn.root, 'A_Scripts/tools/']; addpath(pn.tools);

ID = '0012';
stroopVersion = '1';

pdat.IN = ['/Volumes/LNDG/StateSwitch/D_analyses/A_Stroop/B_DataRaw/',ID,'_StroopData_',stroopVersion,'.mat'];
pdat.OUT = '/Volumes/LNDG/StateSwitch/D_analyses/A_Stroop/C_DataLabeled/';

A_StateSwitch_CARL_labelSegments_170912(pdat, ID)